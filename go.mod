module gitlab.com/forpelevin/go-micro-layout

go 1.12

require (
	github.com/pkg/errors v0.8.1
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	gopkg.in/yaml.v2 v2.2.2
)
