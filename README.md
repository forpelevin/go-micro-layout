# Go micro layout
The project is layout for Golang projects.
## Getting Started
```
git clone https://gitlab.com/forpelevin/go-micro-layout.git
```
## Prerequisites
For the successful using you should have:
```
go >= 1.12
```
## Running the tests
It's a good practice to run the tests before using the app to make sure everything is OK.
```
cd $GOPATH/src/gitlab.com/forpelevin/go-micro-layout 
make test
```
## Build the app
```
cd $GOPATH/src/gitlab.com/forpelevin/go-micro-layout
make build
```
## Sample of using
```
./main
```
## License
This project is licensed under the MIT License.