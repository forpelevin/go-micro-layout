package server

import (
	"errors"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"path"
	"runtime"
)

// Config is a struct that implements the server settings.
type Config struct {
	Host string
	Port string
}

// LogConfig is a struct that implements the log settings
type LogConfig struct {
	Level string
}

// ReadYamlConfig reads a file with provided name in the config folder and return its content.
func ReadYamlConfig(filename string) ([]byte, error) {
	_, currentFilename, _, ok := runtime.Caller(0)
	if !ok {
		return nil, errors.New("cannot get caller information to read configs")
	}
	dir := path.Dir(currentFilename)

	return ioutil.ReadFile(fmt.Sprintf("%s/../../config/%s", dir, filename))
}

// ReadServerConfig reads the server.yaml file from config directory, unmarshals it,
// and returns a filled config structure.
func ReadServerConfig() (*Config, error) {
	content, err := ReadYamlConfig("server.yaml")
	if err != nil {
		return nil, err
	}

	c := &Config{}
	err = yaml.Unmarshal(content, &c)

	return c, err
}

// ReadLogConfig reads the log.yaml file from config directory, unmarshals it,
// and returns a filled config structure.
func ReadLogConfig() (*LogConfig, error) {
	content, err := ReadYamlConfig("log.yaml")
	if err != nil {
		return nil, err
	}

	c := &LogConfig{}
	err = yaml.Unmarshal(content, &c)

	return c, err
}
