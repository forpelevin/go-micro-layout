package server

import (
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"strings"
)

var levelStringToZapLevelMap = map[string]zapcore.Level{
	"debug": zapcore.DebugLevel,
	"info":  zapcore.InfoLevel,
	"warn":  zapcore.WarnLevel,
	"error": zapcore.ErrorLevel,
	"panic": zapcore.PanicLevel,
	"fatal": zapcore.FatalLevel,
}

// MakeLogger reads the log config from log.yaml file and creates a new zap logger instance.
func MakeLogger() (*zap.Logger, error) {
	lc, err := ReadLogConfig()
	if err != nil {
		return nil, err
	}

	lvl, ok := levelStringToZapLevelMap[strings.ToLower(lc.Level)]
	if !ok {
		return nil, errors.New("invalid log level")
	}

	zc := zap.NewProductionConfig()
	zc.Level.SetLevel(lvl)

	return zap.NewProduction()
}
