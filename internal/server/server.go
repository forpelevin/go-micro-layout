// Package server implements a main server of application that works with settings
// and listens for HTTP connections.
package server

import (
	"fmt"
	"log"
	"net/http"
)

// Run creates the server logger, registers HTTP handlers and listens for connections.
func Run() {
	logger, err := MakeLogger()
	if err != nil {
		log.Fatal(err)
	}

	// @TODO Implement your custom handlers
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		logger.Info("Hello world!")
	})

	c, err := ReadServerConfig()
	if err != nil {
		log.Fatal(err)
	}

	addr := fmt.Sprintf("%s:%s", c.Host, c.Port)
	log.Println("Listening on", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
