package main

import "gitlab.com/forpelevin/go-micro-layout/internal/server"

func main() {
	server.Run()
}
